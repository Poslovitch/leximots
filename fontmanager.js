/**
* Handles fonts switching.
*/
// TODO: Improve how the default font is handled. Right now it is get, set, get and set multiple times.
const FontManager = {
  _sessionStorageKey: "fonte",
  _defaultFont: "script", // TODO: make the defaultFont = availableFonts[0] ?
  _availableFonts: ["script", "cursive", "luciole"],
  
  /**
  * Sets the letter font to the specified font.
  * @param {String} font "script" or "cursive".
  */
  setLetterFont: function(font) {
    if (this.isAvailable(font)) {
      document.getElementById("font-stylesheet").href="font-" + font + ".css";
      this.storeFontIntoSessionStorage(font);
    }
  },
  
  /**
  * Loads the default font based on the sessionStorage if it is set, or loads the default font otherwise.
  */
  loadDefaultFont: function() {
    this._defaultFont = this.getFontFromSessionStorage();
    this.setLetterFont(this._defaultFont);
  },

  /**
   * Gets the font stored in the sessionStorage if it is set, or returns {@link _defaultFont} otherwise.
   * @returns the font stored in the sessionStorage if it is set, or {@link _defaultFont} otherwise.
   */
  getFontFromSessionStorage: function() {
    let storedValue = sessionStorage.getItem(this._sessionStorageKey)
    if (storedValue && this.isAvailable(storedValue)) return storedValue;
    return this._defaultFont; // FIXME: Don't do that and return undefined or null instead?
  },

  /**
   * 
   * @param {String} font 
   */
  storeFontIntoSessionStorage: function(font) {
    sessionStorage.setItem(this._sessionStorageKey, font);
  },

  /**
   * 
   * @param {String} font 
   * @returns {boolean} true if the specified font is available, false otherwise.
   */
  isAvailable: function(font) {
    return this._availableFonts.indexOf(font) !== -1;
  },

  getAvailableFonts: function() {
    return this._availableFonts;
  },

  isDefaultFont: function(font) {
    return this._defaultFont === font;
  }
};  